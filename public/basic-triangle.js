const canvas = document.querySelector('canvas');
const gl = canvas.getContext('webgl');

if (!gl) {
  throw new Error('Your browser does not support WebGL.');
}

// Create vertex data

const vertexData = new Float32Array([
  0, 1, 0,
  1, -1, 0,
  -1, -1, 0
]);

// Create buffer

const buffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, buffer);

// Load vertex data into buffer

gl.bufferData(gl.ARRAY_BUFFER, vertexData, gl.STATIC_DRAW);

// Create vertex shader

const vertexShader = gl.createShader(gl.VERTEX_SHADER);
gl.shaderSource(vertexShader, `
attribute vec3 position;
void main() {
  gl_Position = vec4(position, 1);
}
`);
gl.compileShader(vertexShader);

// Create fragment shader

const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
const rnd = [
  Math.random().toFixed(1),
  Math.random().toFixed(1),
  Math.random().toFixed(1),
];
console.log(rnd);
gl.shaderSource(fragmentShader, `
void main() {
  gl_FragColor = vec4(${rnd[0]}, ${rnd[1]}, ${rnd[2]}, 1);
}
`);
gl.compileShader(fragmentShader);

// Create shader program

const program = gl.createProgram();

// Attach shaders to program & link

gl.attachShader(program, vertexShader);
gl.attachShader(program, fragmentShader);
gl.linkProgram(program);

/**
 * Enable vertex attributes (these are the attributes defined in the vertex shader,
 * right now we are only using position). Then we enable it.
 *  */

const positionAttributeRef = gl.getAttribLocation(program, `position`);
gl.enableVertexAttribArray(positionAttributeRef);

/**
 * We explain here how these attributes should be read from our vertexData array.
 * We point to the positionAttributeRef in the first param,
 * in the next one we tell it how many elements of the vertexData make up
 * a single position attribute (3 in this case, because we need x, y & z).
 * Then we give it the type, they are all floats.
 * The last three we ignore for now.
 */
gl.vertexAttribPointer(positionAttributeRef, 3, gl.FLOAT, false, 0, 0);

/**
 * Use program (creates an executable program on gfx card which opengl will use when you call the draw command)
 *  */ 
gl.useProgram(program);

/**
 * Draw the shape.
 * We specify we want to draw triangles.
 * We also specify that we should start at the first vertex,
 * and that we are dealing with three vertices.
 * We know this because our vertexData array describes only three vertices.
 */
gl.drawArrays(gl.TRIANGLES, 0, 3);
