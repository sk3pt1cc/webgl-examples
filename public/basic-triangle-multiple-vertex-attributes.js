const canvas = document.querySelector('canvas');
const gl = canvas.getContext('webgl');

if (!gl) {
  throw new Error('Your browser does not support WebGL.');
}

/**
 * Create vertex data.
 * These vertices will consist of 6 individual points.
 * The first three are the position points,
 * the next three are the colour points (these are generated randomly)
 *  */ 

const vertexData = new Float32Array([
  0, 1, 0,
  1, -1, 0,
  -1, -1, 0,
]);

const colorData = new Float32Array([
  Math.random().toFixed(1), Math.random().toFixed(1), Math.random().toFixed(1),
  Math.random().toFixed(1), Math.random().toFixed(1), Math.random().toFixed(1),
  Math.random().toFixed(1), Math.random().toFixed(1), Math.random().toFixed(1),
]);

// Create position buffer & bind

const positionBuffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
gl.bufferData(gl.ARRAY_BUFFER, vertexData, gl.STATIC_DRAW);

// Create color buffer & bind
const colorBuffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
gl.bufferData(gl.ARRAY_BUFFER, colorData, gl.STATIC_DRAW);

/** 
 * Create vertex shader.
 *  All attributes must be defined within the vertex shader, which means we cant just
 * pass 'color' down to the fragment shader in its current format. Instead we create this
 * 'varying vec3 vertexColor' variable in both shaders and opengl links these together.
 * We also need to set this value in the main function.
 * 'precision mediump float' just sets the precision of floating point numbers.
 *  */

const vertexShader = gl.createShader(gl.VERTEX_SHADER);
gl.shaderSource(vertexShader, `
precision mediump float;
attribute vec3 position;
attribute vec3 color;

varying vec3 vertexColor;

void main() {
  vertexColor = color;
  gl_Position = vec4(position, 1);
}
`);
gl.compileShader(vertexShader);

// Create fragment shader

const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
gl.shaderSource(fragmentShader, `
precision mediump float;
varying vec3 vertexColor;

void main() {
  gl_FragColor = vec4(vertexColor, 1);
}
`);
gl.compileShader(fragmentShader);

// Create shader program

const program = gl.createProgram();

// Attach shaders to program & link

gl.attachShader(program, vertexShader);
gl.attachShader(program, fragmentShader);
gl.linkProgram(program);

/**
 * Enable vertex attributes (these are the attributes defined in the vertex shader,
 * right now we are only using position). Then we enable it.
 *  */

const positionAttributeRef = gl.getAttribLocation(program, `position`);
const colorAttributeRef = gl.getAttribLocation(program, `color`);
gl.enableVertexAttribArray(positionAttributeRef);
gl.enableVertexAttribArray(colorAttributeRef);

/**
 * We explain here how these attributes should be read from our vertexData array.
 * We point to the positionAttributeRef in the first param,
 * in the next one we tell it how many elements of the vertexData make up
 * a single position attribute (3 in this case, because we need x, y & z).
 * Then we give it the type, they are all floats.
 * The last three we ignore for now.
 * 
 * Also note that when using this, we have to have the correct buffer bound.
 * Right now the last bound buffer was the color buffer, so these calls are
 * going to try to read from that. We want the first one to read from position, though
 * and the second one to read from colorBuffer. So that's why we bind the buffers
 * before each call.
 */
gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
gl.vertexAttribPointer(positionAttributeRef, 3, gl.FLOAT, false, 0, 0);

gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
gl.vertexAttribPointer(colorAttributeRef, 3, gl.FLOAT, false, 0, 0);

/**
 * Use program (creates an executable program on gfx card which opengl will use when you call the draw command)
 *  */ 
gl.useProgram(program);

/**
 * Draw the shape.
 * We specify we want to draw triangles.
 * We also specify that we should start at the first vertex,
 * and that we are dealing with three vertices.
 * We know this because our vertexData array describes only three vertices.
 */
gl.drawArrays(gl.TRIANGLES, 0, 3);
